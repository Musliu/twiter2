class HomeController < ApplicationController
  before_action :set_auth # kjo tregon qe para cdo metode e para do te ekzekutohet set_auth metoda
  def index
  end

  def profile
  end

  private

  def set_auth
    @auth = session[:omniauth] if session[:omniauth]
  end
end
